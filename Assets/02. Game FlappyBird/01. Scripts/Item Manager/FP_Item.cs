﻿using UnityEngine;
using System.Collections;

public class FP_Item : MonoBehaviour {

	private FP_Player					player;
	private	GameObject					pointDestroy;
	public 	Vector3						destination;
	public	float						velocity;
	public	FP_ItemManager.FP_IM_Type	type;
	
	// Use this for initialization
	void Start () 
	{
		player			= GameObject.FindGameObjectWithTag("FP_Player").GetComponent<FP_Player>();
		pointDestroy 	= GameObject.FindGameObjectWithTag("FP_PointDestroy");
		destination		= pointDestroy.transform.position;
		destination.y	= transform.position.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!player.dead) 
		{
			//transform.position = Vector3.Lerp(transform.position, destination, 0.3f*Time.deltaTime);
			float _velocity = -velocity;
			if (player.modeSpeed)
			{
				_velocity *= player.speedFactor;
			}
			transform.Translate(new Vector2 (_velocity*Time.deltaTime,0.0f));
			
			if (transform.position.x <= pointDestroy.transform.position.x) {
				Destroy(gameObject);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.gameObject.tag == "FP_Player") 
		{
			if (!player.dead)
			{
				CallManager();
				Destroy(this.gameObject);
			}
		}
	} 

	void CallManager()
	{
		Debug.Log ("CallManager:" + type);
		switch (type) 
		{
		case FP_ItemManager.FP_IM_Type.Rain:
			GameObject rainManager = GameObject.FindGameObjectWithTag("FP_RainManager");
			rainManager.GetComponent<FP_RainManager>().StartRain();
			break;
		case FP_ItemManager.FP_IM_Type.Gravity:
			GameObject gravityManager = GameObject.FindGameObjectWithTag("FP_GravityManager");
			gravityManager.GetComponent<FP_GravityManager>().StartGravity();
			break;
		case FP_ItemManager.FP_IM_Type.Speed:
			GameObject speedManager = GameObject.FindGameObjectWithTag("FP_SpeedManager");
			speedManager.GetComponent<FP_SpeedManager>().StartSpeed();
			break;
		}

	}
}
