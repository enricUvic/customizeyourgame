﻿using UnityEngine;
using System.Collections;

public class FP_RainManager : MonoBehaviour {

	public	AudioClip		loopRain;
	public	GameObject		rain;
	public	float			lifeTime = 8.0f;
	private FP_Player		player;
	private float			timerRain;
	private bool			startRain;
	private bool			fadeInGrey;
	private bool			fadeOutGrey;
	private float			fadeTimer;

	void Awake ()
	{
		rain.SetActive (false);
	}

	// Use this for initialization
	void Start () 
	{
		fadeInGrey	= false;
		fadeOutGrey	= false;
		player		= GameObject.FindGameObjectWithTag("FP_Player").GetComponent<FP_Player>();
		startRain	= false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (startRain) 
		{
			timerRain += Time.deltaTime;
			if (timerRain > lifeTime)
			{
				StopRain();
			}
		}

		if (fadeInGrey) {
			fadeTimer +=Time.deltaTime;
			if (fadeTimer <= 2.0f)
			{
				Camera.main.backgroundColor = Color.Lerp(Color.white,Color.gray,fadeTimer/2.0f);
			}
			else{
				fadeInGrey = false;
			}
		}

		if (fadeOutGrey) {
			fadeTimer +=Time.deltaTime;
			if (fadeTimer <= 2.0f)
			{
				Camera.main.backgroundColor = Color.Lerp(Color.gray,Color.white,fadeTimer/2.0f);
			}
			else{
				fadeOutGrey = false;
			}
		}
	}


	public void StartRain()
	{
		rain.SetActive (true);
		startRain = true;
		fadeInGrey = true;
		fadeOutGrey = false;
		timerRain = 0.0f;
		fadeTimer = 0.0f;
		rain.GetComponent<ParticleSystem>().Play ();
		player.GetComponent<AudioSource>().PlayOneShot (loopRain);
		player.modeRain = true;
	}


	public void StopRain()
	{
		fadeTimer = 0.0f;
		fadeInGrey = false;
		fadeOutGrey = true;
		startRain = false;
		rain.GetComponent<ParticleSystem>().Stop ();
		player.GetComponent<AudioSource>().Stop ();
		player.modeRain = false;
	}
}
