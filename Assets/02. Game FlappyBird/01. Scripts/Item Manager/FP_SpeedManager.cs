﻿using UnityEngine;
using System.Collections;

public class FP_SpeedManager : MonoBehaviour
{
	public	float			lifeTime = 8.0f;
	public	float			speedFactor = 1.5f;
	private FP_Player		player;
	private	bool			startSpeed;
	private float			timerSpeed;
	// Use this for initialization
	void Start () 
	{
		player		= GameObject.FindGameObjectWithTag("FP_Player").GetComponent<FP_Player>();
		startSpeed	= false;

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (startSpeed) 
		{
			timerSpeed += Time.deltaTime;
			if (timerSpeed > lifeTime)
			{
				StopSpeed();
			}
		}
	}

	public void StartSpeed()
	{

		startSpeed = true;
		timerSpeed = 0.0f;
		player.modeSpeed = true;
		player.speedFactor = speedFactor;
	}
	
	
	public void StopSpeed()
	{
		startSpeed = false;
		player.modeSpeed = false;
	}
}
