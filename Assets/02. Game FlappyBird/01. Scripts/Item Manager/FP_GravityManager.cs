﻿using UnityEngine;
using System.Collections;

public class FP_GravityManager : MonoBehaviour 
{
	private FP_Player	player;
	public float		timerGravity;
	public bool			start;

	void Awake ()
	{
	}

	// Use this for initialization
	void Start () 
	{
		timerGravity = 0.0f;
		start = false;
		player = GameObject.FindGameObjectWithTag("FP_Player").GetComponent<FP_Player>();

		Vector2 gravity = Physics2D.gravity;
		if (gravity.y > 0) {
			gravity.y = -gravity.y;
			Physics2D.gravity = gravity;
		}


	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.G)) {
			StartGravity();
		}

		if (start) {
			timerGravity += Time.deltaTime;
			if (timerGravity > 8.0f)
			{
				timerGravity = 0.0f;
				StopGravity();
			}
		}
	}


	public void StartGravity()
	{
		Vector2 gravity = Physics2D.gravity;
		gravity.y = -gravity.y;
		Physics2D.gravity = gravity;
		start = true;
		player.modeGravity = true;

	}

	public void StopGravity ()
	{
		Vector2 gravity = Physics2D.gravity;
		gravity.y = -gravity.y;
		Physics2D.gravity = gravity;
		start = false;
		player.modeGravity = false;
	}
}
