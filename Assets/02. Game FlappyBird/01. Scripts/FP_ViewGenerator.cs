﻿using UnityEngine;
using System.Collections;

public class FP_ViewGenerator : MonoBehaviour 
{
	private	FP_Player	player;
	public float 		speedLand;
	public float 		speedMountain;
	public float 		speedMountainF;
	public GameObject 	land_1;
	public GameObject 	land_2;
	public GameObject 	mountain_1;
	public GameObject 	mountain_2;
	public GameObject 	mountainF_1;
	public GameObject 	mountainF_2;
	public Transform 	pointDest;
	private float 		sizeLand = 0.0f;
	private float 		sizeMountain = 0.0f;
	private float 		sizeMountainF = 0.0f;
	public bool			start = false;
	public GameObject	prefabCloud1;
	public GameObject	prefabCloud2;
	public float		timerCloud = 5.0f;
	public GameObject	cloudPointGenerator;


	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("FP_Player").GetComponent<FP_Player>();
		sizeLand = (float)(land_1.GetComponent<Collider2D> ().bounds.max.x - land_1.GetComponent<Collider2D> ().bounds.min.x);
		sizeMountain = (float)(mountain_1.GetComponent<Collider2D> ().bounds.max.x - mountain_1.GetComponent<Collider2D> ().bounds.min.x);
		sizeMountainF = (float)(mountainF_1.GetComponent<Collider2D> ().bounds.max.x - mountainF_1.GetComponent<Collider2D> ().bounds.min.x);
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateClouds ();

		if (!start)
		{
			return;
		}



		MoveSprites ();

		CheckJump (mountain_1, mountain_2, sizeMountain);
		CheckJump (mountainF_1, mountainF_2, sizeMountainF);
		CheckJump (land_1, land_2, sizeLand);
	}

	void UpdateClouds()
	{
		timerCloud += Time.deltaTime;
		if (timerCloud > 5.0f) 
		{
			timerCloud = 0.0f;
			Vector3 position = new Vector3();
			position = cloudPointGenerator.transform.position;
			position.y += Random.Range(0,1.5f);
			GameObject cloud;
			if(Random.Range(0,2) == 0)
			{
				cloud = Instantiate(prefabCloud1, position, Quaternion.identity) as GameObject;
			}else{
				cloud = Instantiate(prefabCloud2, position, Quaternion.identity) as GameObject;
			}

			cloud.GetComponent<FP_Cloud>().pointDestruction = pointDest.position;
		}
	}

	void MoveSprites ()
	{
		float _speedLand 		= speedLand;
		float _speedMountain	= speedMountain;
		float _speedMountainF 	= speedMountainF;

		if (player.modeSpeed) 
		{
			_speedLand 		*= player.speedFactor;
			_speedMountain 	*= player.speedFactor;
			_speedMountainF *= player.speedFactor;
		}

		//-----Move Lands:
		land_1.transform.Translate (new Vector2 (-Time.deltaTime * _speedLand, 0.0f));
		land_2.transform.Translate (new Vector2 (-Time.deltaTime * _speedLand, 0.0f));

		//-----Move Mountains:
		mountain_1.transform.Translate (new Vector2 (-Time.deltaTime * _speedMountain, 0.0f));
		mountain_2.transform.Translate (new Vector2 (-Time.deltaTime * _speedMountain, 0.0f));

		//-----Move Mountains Far:
		mountainF_1.transform.Translate (new Vector2 (-Time.deltaTime * _speedMountainF, 0.0f));
		mountainF_2.transform.Translate (new Vector2 (-Time.deltaTime * _speedMountainF, 0.0f));
	}



	void CheckJump (GameObject sprite1, GameObject sprite2, float size)
	{
		if (sprite1.transform.position.x - size * 0.5f < pointDest.position.x) 
		{
			float posX = sprite2.transform.position.x + size;
			Vector2 pos = sprite1.transform.position;
			pos.x = posX;
			sprite1.transform.position = pos;
		}
		
		if (sprite2.transform.position.x - size * 0.5f < pointDest.position.x) 
		{
			float posX = sprite1.transform.position.x + size;
			Vector2 pos = sprite2.transform.position;
			pos.x = posX;
			sprite2.transform.position = pos;
		}
	}

}
