﻿using UnityEngine;
using System.Collections;

public class FP_Cloud : MonoBehaviour {

	public Vector3	pointDestruction;
	public float	velocity = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate(new Vector2 (-velocity*Time.deltaTime,0.0f));

		if (transform.position.x <= pointDestruction.x)
		{
			Destroy(gameObject);
		}
	}
}
