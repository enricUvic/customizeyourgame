﻿using UnityEngine;
using System.Collections;

public class FP_Generator : MonoBehaviour {

	public FP_Player			player;
	public GameObject			prefabPipe;
	public GameObject			prefabItem;
	public float				timeToGenerate = 3.0f;
	public float				speedPipe;
	public float				timer;
	public GameObject			pointGenerator;
	public float				maxY;
	public float				minY;
	public bool					start;
	private FP_ViewGenerator	viewGenerator;
	public int					numPipes = 0;
	public float				randomY = -1;

	// Use this for initialization
	void Start () 
	{
		numPipes = 0;
		start = false;
		timer 	= 0.0f; 
		viewGenerator = GameObject.FindGameObjectWithTag("FP_ViewGenerator").GetComponent<FP_ViewGenerator>();
		viewGenerator.speedLand = speedPipe;
		viewGenerator.speedMountain = speedPipe * 0.3f;
		viewGenerator.speedMountainF = speedPipe * 0.2f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!start) {
			return;
		}

		timer -= Time.deltaTime;
		if (timer < 0.0f && !player.dead)
		{
			randomY = Random.Range(minY,maxY);
			GameObject pipeObj = Instantiate(prefabPipe, new Vector2(pointGenerator.transform.position.x, randomY), Quaternion.identity) as GameObject;
			pipeObj.GetComponent<FP_Pipe>().velocity = speedPipe;
			timer = timeToGenerate;
			numPipes++;

		}
	}


	public void CreateItem(FP_ItemManager.FP_IM_Type type)
	{
		GameObject obj = Instantiate(prefabItem, new Vector2(pointGenerator.transform.position.x, randomY), Quaternion.identity) as GameObject;
		obj.GetComponent<FP_Item>().velocity = speedPipe;
		obj.GetComponent<FP_Item>().type = type;
	}
}
