﻿using UnityEngine;
using System.Collections;

public class FP_Pipe : MonoBehaviour {

	private FP_Player	player;
	private	GameObject 	pointDestroy;
	public 	Vector3 	destination;
	public	float 		velocity;

	// Use this for initialization
	void Start () 
	{
		player			= GameObject.FindGameObjectWithTag("FP_Player").GetComponent<FP_Player>();
		pointDestroy 	= GameObject.FindGameObjectWithTag("FP_PointDestroy");
		destination		= pointDestroy.transform.position;
		destination.y = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!player.dead) 
		{

			//transform.position = Vector3.Lerp(transform.position, destination, 0.3f*Time.deltaTime);
			float _velocity = -velocity;
			if (player.modeSpeed)
			{
				_velocity *= player.speedFactor;
			}
			transform.Translate(new Vector2 (_velocity*Time.deltaTime,0.0f));
			
			if (transform.position.x <= pointDestroy.transform.position.x) {
				Destroy(gameObject);
			}
		}
	}


}
