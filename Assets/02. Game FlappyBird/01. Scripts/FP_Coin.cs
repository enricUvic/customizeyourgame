﻿using UnityEngine;
using System.Collections;

public class FP_Coin : MonoBehaviour {

	private	FP_Score 		score;
	private FP_Player		player;

	// Use this for initialization
	void Start () {
		score = GameObject.FindGameObjectWithTag ("FP_Score").GetComponent<FP_Score>();
		player = GameObject.FindGameObjectWithTag ("FP_Player").GetComponent<FP_Player> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.gameObject.tag == "FP_Player") 
		{
			if (!player.dead)
			{
				score.AddScore();
				player.SetSmileFace();
				Destroy(this.gameObject);
			}

		}
	} 
}
